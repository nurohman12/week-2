<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");

echo $sheep->getName(); // shaun
echo $sheep->getLegs(); // 4
echo $sheep->getCold_Blooded(); // no
echo "<br>";
$kodok = new Frog("beduk");
echo $kodok->getName();
echo $kodok->getLegs();
echo $kodok->getCold_Blooded();
echo $kodok->jump();
echo "<br>";
$sungokong = new Ape("kera sakti");
echo $sungokong->getName();
echo $sungokong->getLegs();
echo $sungokong->getCold_Blooded();
echo $sungokong->yell();
